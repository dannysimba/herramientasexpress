from django.apps import AppConfig


class HerramientasexpressConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'Aplicacion.HerramientasExpress'
