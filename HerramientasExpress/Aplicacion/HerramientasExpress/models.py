from django.db import models

class Categoria(models.Model):
    idca_SG = models.AutoField(primary_key=True)
    nombre_SG = models.CharField(max_length=50)
    descripcion_SG = models.CharField(max_length=150)
    fechaCreacion_SG = models.DateField()
    ubicacion_SG = models.TextField()

    def __str__(self):
        fila="{0}: {1}"
        return fila.format(self.idca_SG,self.nombre_SG)


class Provedores(models.Model):
    idpo_SG = models.AutoField(primary_key=True)
    nombre_SG = models.CharField(max_length=150)
    direccion_SG = models.TextField()
    ciudad_SG = models.TextField()
    correo_SG = models.EmailField()
    telefono_SG = models.CharField(max_length=20)
    fotografia_SG=models.FileField(upload_to='provedores', null=True,blank=True)
    def __str__(self):
        fila="{0}: {1}"
        return fila.format(self.idpo_SG,self.nombre_SG)


class Productos(models.Model):
    idpr_SG = models.AutoField(primary_key=True)
    nombre_SG = models.CharField(max_length=150)
    descripcion_SG = models.CharField(max_length=150)
    precio_SG = models.DecimalField(max_digits=10, decimal_places=2)
    cantidad_SG = models.IntegerField()
    fotografia_SG=models.FileField(upload_to='productos', null=True,blank=True)
    categoria = models.ForeignKey(Categoria, null=True, blank=True, on_delete=models.PROTECT)
    provedor = models.ForeignKey(Provedores, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila="{0}: {1}"
        return fila.format(self.idpr_SG,self.nombre_SG)


class Clientes(models.Model):
    idcli_SG = models.AutoField(primary_key=True)
    nombre_SG = models.CharField(max_length=150)
    telefono_SG = models.CharField(max_length=20)
    direccion_SG = models.TextField()
    correo_SG = models.EmailField()
    cedula_SG=models.FileField(upload_to='clientes', null=True,blank=True)
    def __str__(self):
        fila="{0}: {1}"
        return fila.format(self.idcli_SG,self.nombre_SG)


class Ventas(models.Model):
    idve_SG = models.AutoField(primary_key=True)
    tipo_SG = models.CharField(max_length=150)
    fecha_SG = models.CharField(max_length=150)
    precioUnitario_SG = models.CharField(max_length=150)
    precioTotal_SG = models.CharField(max_length=150)
    descripcion_SG = models.CharField(max_length=150)
    clientes = models.ForeignKey(Clientes, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila="{0}: {1}"
        return fila.format(self.idve_SG,self.tipo_SG)


class Detalles(models.Model):
    idde_SG = models.AutoField(primary_key=True)
    cantidad_SG = models.CharField(max_length=150)
    precio_SG = models.CharField(max_length=150)
    descripcion_SG = models.CharField(max_length=150)
    ventas = models.ForeignKey(Ventas, null=True, blank=True, on_delete=models.PROTECT)
    productos = models.ForeignKey(Productos, null=True, blank=True, on_delete=models.PROTECT)

    def __str__(self):
        fila="{0}: {1}"
        return fila.format(self.idde_SG,self.cantidad_SG)
