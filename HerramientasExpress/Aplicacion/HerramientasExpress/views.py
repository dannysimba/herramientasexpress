from django.shortcuts import render, redirect, HttpResponseRedirect
from .models import *
from django.core.mail import send_mail
from django.contrib import messages
from django.conf import settings
from django.db.models.deletion import ProtectedError
# Create your views here.

def inicio(request):
    return render(request, 'inicio.html')

def vistal(request):
    return render(request, 'enviar_correo.html')

def enviar_correo(request):
    if request.method == 'POST':
        destinatario = request.POST.get('destinatario')
        asunto = request.POST.get('asunto')
        cuerpo = request.POST.get('cuerpo')

        send_mail(asunto, cuerpo, settings.EMAIL_HOST_USER, [destinatario], fail_silently=False)

        messages.success(request, 'Se ha enviado correo')
        return HttpResponseRedirect('/vistal')

    return render(request, 'enviar_correo.html')
#####################################################################################################################################
def listadoClientes(request):
    clienteBdd = Clientes.objects.all()
    return render(request, 'listadoClientes.html', {'clientes': clienteBdd})

def guardarClientes(request):
    nombre_SG=request.POST["nombre_SG"]
    telefono_SG=request.POST["telefono_SG"]
    direccion_SG=request.POST["direccion_SG"]
    correo_SG=request.POST["correo_SG"]
    cedula_SG=request.FILES.get("cedula_SG")
    nuevoClientes=Clientes.objects.create(nombre_SG=nombre_SG,telefono_SG=telefono_SG, direccion_SG=direccion_SG, correo_SG=correo_SG, cedula_SG=cedula_SG)
    messages.success(request,'Cliente guardado exitosamente')
    return redirect('listadoClientes')

def eliminarClientes(request, idcli_SG):
    try:
        cliente = Clientes.objects.get(pk=idcli_SG)
        cliente.delete()
        messages.success(request, 'Cliente eliminado correctamente.')
    except Clientes.DoesNotExist:
        messages.error(request, 'El Cliente que intentas eliminar no existe.')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar el cliente porque hay productos relacionados.')
    return redirect('listadoClientes')

def editarClientes(request,idcli_SG):
    clienteEditar = Clientes.objects.get(idcli_SG=idcli_SG)
    return render(request, 'editarClientes.html',{'clientes':clienteEditar})

def procesarActualizacionCClientes(request):
    idcli_SG=request.POST["idcli_SG"]
    nombre_SG=request.POST["nombre_SG"]
    telefono_SG=request.POST["telefono_SG"]
    direccion_SG=request.POST["direccion_SG"]
    correo_SG=request.POST["correo_SG"]
    cedula_SG=request.FILES.get("cedula_SG")
    #Insertando datos mediante el ORM de DJANGO
    clienteEditar=Clientes.objects.get(idcli_SG=idcli_SG)
    clienteEditar.nombre_SG=nombre_SG
    clienteEditar.telefono_SG=telefono_SG
    clienteEditar.direccion_SG=direccion_SG
    clienteEditar.correo_SG=correo_SG
    clienteEditar.cedula_SG=cedula_SG
    clienteEditar.save()
    messages.success(request,
      'Cliente ACTUALIZADO Exitosamente')
    return redirect('listadoClientes')
#######################################################################################################################################################################
def listadoProductos(request):
    productoBdd = Productos.objects.all()
    categoriaBdd = Categoria.objects.all()
    provedorBdd = Provedores.objects.all()
    return render(request, 'listadoProductos.html', {'productos': productoBdd,'categorias':categoriaBdd,'provedores':provedorBdd })

def guardarProductos(request):
    idca_SG_categoria=request.POST["idca_SG_categoria"]
    categoriaSeleccionado=Categoria.objects.get(idca_SG=idca_SG_categoria)
    idpo_SG_provedor=request.POST["idpo_SG_provedor"]
    provedorSeleccionado=Provedores.objects.get(idpo_SG=idpo_SG_provedor)
    nombre_SG=request.POST["nombre_SG"]
    descripcion_SG=request.POST["descripcion_SG"]
    precio_SG=request.POST["precio_SG"]
    cantidad_SG=request.POST["cantidad_SG"]
    fotografia_SG=request.FILES.get("fotografia_SG")
    nuevoProductos=Productos.objects.create(nombre_SG=nombre_SG, descripcion_SG=descripcion_SG, precio_SG=precio_SG, cantidad_SG=cantidad_SG, fotografia_SG=fotografia_SG, categoria=categoriaSeleccionado, provedor=provedorSeleccionado)
    messages.success(request,'Producto guardado exitosamente')
    return redirect('listadoProductos')

def eliminarProductos(request, idpr_SG):
    try:
        producto = Productos.objects.get(pk=idpr_SG)
        producto.delete()
        messages.success(request, 'Productos eliminado correctamente.')
    except Provedores.DoesNotExist:
        messages.error(request, 'El producto que intentas eliminar no existe.')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar el producto porque hay productos relacionados.')
    return redirect('listadoProductos')

def editarProductos(request,idpr_SG):
    productoEditar=Productos.objects.get(idpr_SG=idpr_SG)
    categoriaBdd = Categoria.objects.all()
    provedorBdd = Provedores.objects.all()
    return render(request, 'editarProductos.html',{'productos':productoEditar, 'categorias':categoriaBdd,'provedores':provedorBdd})

def procesarActualizacionProductos(request):
    idpr_SG=request.POST["idpr_SG"]
    idca_SG_categoria=request.POST["idca_SG_categoria"]
    categoriaSeleccionado=Categoria.objects.get(idca_SG=idca_SG_categoria)
    idpo_SG_provedor=request.POST["idpo_SG_provedor"]
    provedorSeleccionado=Provedores.objects.get(idpo_SG=idpo_SG_provedor)
    nombre_SG=request.POST["nombre_SG"]
    descripcion_SG=request.POST["descripcion_SG"]
    precio_SG=request.POST["precio_SG"]
    cantidad_SG=request.POST["cantidad_SG"]
    fotografia_SG=request.FILES.get("fotografia_SG")
    #Insertando datos mediante el ORM de DJANGO
    productoEditar=Productos.objects.get(idpr_SG=idpr_SG)
    productoEditar.categoria=categoriaSeleccionado
    productoEditar.provedor=provedorSeleccionado
    productoEditar.nombre_SG=nombre_SG
    productoEditar.descripcion_SG=descripcion_SG
    productoEditar.precio_SG=precio_SG
    productoEditar.cantidad_SG=cantidad_SG
    productoEditar.fotografia_SG=fotografia_SG
    productoEditar.save()
    messages.success(request,
      'Producto ACTUALIZADO Exitosamente')
    return redirect('listadoProductos')
#############################################################################################################################################################################3

def listadoProvedores(request):
    provedorBdd = Provedores.objects.all()
    return render(request, 'listadoProvedores.html', {'provedores': provedorBdd})

def guardarProvedores(request):
    nombre_SG=request.POST["nombre_SG"]
    direccion_SG=request.POST["direccion_SG"]
    ciudad_SG=request.POST["ciudad_SG"]
    correo_SG=request.POST["correo_SG"]
    telefono_SG=request.POST["telefono_SG"]
    fotografia_SG=request.FILES.get("fotografia_SG")
    nuevoProvedores=Provedores.objects.create(nombre_SG=nombre_SG, direccion_SG=direccion_SG, ciudad_SG=ciudad_SG, correo_SG=correo_SG, telefono_SG=telefono_SG, fotografia_SG=fotografia_SG)
    messages.success(request,'Provedores guardado exitosamente')
    return redirect('listadoProvedores')

def eliminarProvedores(request, idpo_SG):
    try:
        proveedor = Provedores.objects.get(pk=idpo_SG)
        proveedor.delete()
        messages.success(request, 'Proveedor eliminado correctamente.')
    except Provedores.DoesNotExist:
        messages.error(request, 'El proveedor que intentas eliminar no existe.')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar el proveedor porque hay productos relacionados.')
    return redirect('listadoProvedores')

def editarProvedores(request,idpo_SG):
    provedorEditar=Provedores.objects.get(idpo_SG=idpo_SG)
    return render(request, 'editarProvedores.html', {'provedores': provedorEditar})

def procesarActualizacionProvedores(request):
    idpo_SG=request.POST["idpo_SG"]
    nombre_SG=request.POST["nombre_SG"]
    direccion_SG=request.POST["direccion_SG"]
    ciudad_SG=request.POST["ciudad_SG"]
    correo_SG=request.POST["correo_SG"]
    telefono_SG=request.POST["telefono_SG"]
    fotografia_SG=request.FILES.get("fotografia_SG")
    #Insertando datos mediante el ORM de DJANGO
    provedorEditar=Provedores.objects.get(idpo_SG=idpo_SG)
    provedorEditar.nombre_SG=nombre_SG
    provedorEditar.direccion_SG=direccion_SG
    provedorEditar.ciudad_SG=ciudad_SG
    provedorEditar.correo_SG=correo_SG
    provedorEditar.telefono_SG=telefono_SG
    provedorEditar.fotografia_SG=fotografia_SG
    provedorEditar.save()
    messages.success(request,
      'Provedores ACTUALIZADO Exitosamente')
    return redirect('listadoProvedores')
###################################################################################################################################################################

def listadoVentas(request):
    ventasBdd=Ventas.objects.all()
    clientesBdd=Clientes.objects.all()
    return render(request, 'listadoVentas.html', {'ventas':ventasBdd, 'clientes':clientesBdd})

def guardarVentas(request):
    idcli_SG_clientes=request.POST["idcli_SG_clientes"]
    clientesSeleccionado=Clientes.objects.get(idcli_SG=idcli_SG_clientes)
    tipo_SG = request.POST["tipo_SG"]
    fecha_SG = request.POST["fecha_SG"]
    precioUnitario_SG = request.POST["precioUnitario_SG"]
    precioTotal_SG = request.POST["precioTotal_SG"]
    descripcion_SG = request.POST["descripcion_SG"]
    nuevoVentas = Ventas.objects.create(
        tipo_SG=tipo_SG,
        fecha_SG=fecha_SG,
        precioUnitario_SG=precioUnitario_SG,
        precioTotal_SG=precioTotal_SG,
        descripcion_SG=descripcion_SG,
        clientes=clientesSeleccionado
    )
    messages.success(request, 'Venta Guardada Exitosamente')
    return redirect('listadoVentas')


def eliminarVentas(request, idve_SG):
    try:
        ventas = Ventas.objects.get(pk=idve_SG)
        ventas.delete()
        messages.success(request, 'Ventas eliminado correctamente.')
    except Ventas.DoesNotExist:
        messages.error(request, 'El ventas que intentas eliminar no existe.')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar la venta porque hay productos relacionados.')
    return redirect('listadoVentas')

def editarVentas(request, idve_SG):
    ventaEditar = Ventas.objects.get(idve_SG=idve_SG)
    clientesBdd=Clientes.objects.all()
    return render(request, 'editarVentas.html', {'venta': ventaEditar, 'clientes': clientesBdd})

def procesarActualizacionVentas(request):
    idve_SG = request.POST["idve_SG"]
    idcli_SG_clientes = request.POST["idcli_SG_clientes"]
    clientesSeleccionado = Clientes.objects.get(idcli_SG=idcli_SG_clientes)
    tipo_SG = request.POST["tipo_SG"]
    fecha_SG = request.POST["fecha_SG"]
    precioUnitario_SG = request.POST["precioUnitario_SG"]
    precioTotal_SG = request.POST["precioTotal_SG"]
    descripcion_SG = request.POST["descripcion_SG"]

    ventasEditar = Ventas.objects.get(idve_SG=idve_SG)
    ventasEditar.clientes = clientesSeleccionado
    ventasEditar.tipo_SG = tipo_SG
    ventasEditar.fecha_SG = fecha_SG
    ventasEditar.precioUnitario_SG = precioUnitario_SG
    ventasEditar.precioTotal_SG = precioTotal_SG
    ventasEditar.descripcion_SG = descripcion_SG

    ventasEditar.save()
    messages.success(request, 'Venta ACTUALIZADA Exitosamente')
    return redirect('listadoVentas')



def listadoDetalles(request):
    detallesBdd=Detalles.objects.all()
    ventasBdd=Ventas.objects.all()
    productosBdd=Productos.objects.all()

    return render(request, 'listadoDetalles.html', {'detalles':detallesBdd, 'ventas':ventasBdd, 'productos':productosBdd})

def guardarDetalles(request):
    idve_SG_ventas=request.POST["idve_SG_ventas"]
    ventasSeleccionado=Ventas.objects.get(idve_SG=idve_SG_ventas)
    idpr_SG_productos=request.POST["idpr_SG_productos"]
    productosSeleccionado=Productos.objects.get(idpr_SG=idpr_SG_productos)
    cantidad_SG = request.POST["cantidad_SG"]
    precio_SG= request.POST["precio_SG"]
    descripcion_SG = request.POST["descripcion_SG"]
    nuevoDetalles = Detalles.objects.create(
        cantidad_SG=cantidad_SG,
        precio_SG=precio_SG,
        descripcion_SG=descripcion_SG,
        ventas=ventasSeleccionado,
        productos=productosSeleccionado,
    )
    messages.success(request, 'Detalle Guardado Exitosamente')
    return redirect('listadoDetalles')


def eliminarDetalles(request,idde_SG):
    detallesEliminar=Detalles.objects.get(idde_SG=idde_SG)
    detallesEliminar.delete()
    messages.success(request,'Detalle Eliminado Exitosamente')
    return redirect('listadoDetalles')

def editarDetalles(request, idde_SG):
    detalleEditar = Detalles.objects.get(idde_SG=idde_SG)
    ventasBdd = Ventas.objects.all()
    productosBdd = Productos.objects.all()
    return render(request, 'editarDetalles.html', {'detalle': detalleEditar, 'ventas': ventasBdd, 'productos': productosBdd})



def procesarActualizacionDetalles(request):
    idde_SG = request.POST["idde_SG"]
    idve_SG_ventas=request.POST["idve_SG_ventas"]
    ventasSeleccionado=Ventas.objects.get(idve_SG=idve_SG_ventas)
    idpr_SG_productos=request.POST["idpr_SG_productos"]
    productosSeleccionado=Productos.objects.get(idpr_SG=idpr_SG_productos)
    cantidad_SG= request.POST["cantidad_SG"]
    precio_SG = request.POST["precio_SG"]
    descripcion_SG = request.POST["descripcion_SG"]


    detallesEditar = Detalles.objects.get(idde_SG=idde_SG)
    detallesEditar.ventas=ventasSeleccionado
    detallesEditar.productos=productosSeleccionado
    detallesEditar.cantidad_SG = cantidad_SG
    detallesEditar.precio_SG = precio_SG
    detallesEditar.descripcion_SG = descripcion_SG
    detallesEditar.save()
    messages.success(request, 'Detalle ACTUALIZADO Exitosamente')
    return redirect('listadoDetalles')
############################################################################################################################################################

def listadoCategoria(request):
        categoriasBdd=Categoria.objects.all()
        return render(request, 'listadoCategoria.html', {'categorias':categoriasBdd})

def guardarCategoria(request):
        nombre_SG = request.POST["nombre_SG"]
        descripcion_SG = request.POST["descripcion_SG"]
        fechaCreacion_SG= request.POST["fechaCreacion_SG"]
        ubicacion_SG= request.POST["ubicacion_SG"]
        nuevoCategoria = Categoria.objects.create(
            nombre_SG=nombre_SG,
            descripcion_SG=descripcion_SG,
            fechaCreacion_SG=fechaCreacion_SG,
            ubicacion_SG=ubicacion_SG
        )
        messages.success(request, 'Categoria Guardada Exitosamente')
        return redirect('listadoCategoria')


def eliminarCategoria(request, idca_SG):
    try:
        categoria = Categoria.objects.get(pk=idca_SG)
        categoria.delete()
        messages.success(request, 'Categoria se a eliminado correctamente.')
    except Categoria.DoesNotExist:
        messages.error(request, 'La categoria que intentas eliminar no existe.')
    except ProtectedError:
        messages.error(request, 'No se puede eliminar la categoria porque hay productos relacionados.')
    return redirect('listadoCategoria')


def editarCategoria(request, idca_SG):
        categoriaEditar = Categoria.objects.get(idca_SG=idca_SG)
        return render(request, 'editarCategoria.html', {'categoria': categoriaEditar})

def procesarActualizacionCategoria(request):
        idca_SG = request.POST["idca_SG"]
        nombre_SG= request.POST["nombre_SG"]
        descripcion_SG = request.POST["descripcion_SG"]
        fechaCreacion_SG = request.POST["fechaCreacion_SG"]
        ubicacion_SG = request.POST["ubicacion_SG"]


        categoriaEditar = Categoria.objects.get(idca_SG=idca_SG)
        categoriaEditar.nombre_SG = nombre_SG
        categoriaEditar.descripcion_SG = descripcion_SG
        categoriaEditar.fechaCreacion_SG = fechaCreacion_SG
        categoriaEditar.ubicacion_SG = ubicacion_SG


        categoriaEditar.save()
        messages.success(request, 'Categoria ACTUALIZADA Exitosamente')
        return redirect('listadoCategoria')
