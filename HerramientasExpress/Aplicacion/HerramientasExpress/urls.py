from django.urls import path
from . import views
urlpatterns = [

    path('',views.inicio),

    path('listadoClientes/', views.listadoClientes, name='listadoClientes'),
    path('eliminarClientes/<idcli_SG>' ,views.eliminarClientes),
    path('editarClientes/<int:idcli_SG>/', views.editarClientes, name='editarClientes'),
    path('procesarActualizacionCClientes/',views.procesarActualizacionCClientes, name='procesarActualizacionCClientes'),

    path('listadoProductos/', views.listadoProductos, name='listadoProductos'),
    path('guardarProductos/',views.guardarProductos),
    path('eliminarProductos/<idpr_SG>' ,views.eliminarProductos),
    path('editarProductos/<int:idpr_SG>/', views.editarProductos, name='editarProductos'),
    path('procesarActualizacionProductos/',views.procesarActualizacionProductos, name='procesarActualizacionProductos'),

    path('listadoProvedores/', views.listadoProvedores, name='listadoProvedores'),
    path('guardarProvedores/',views.guardarProvedores),
    path('eliminarProvedores/<idpo_SG>' ,views.eliminarProvedores),
    path('editarProvedores/<int:idpo_SG>/', views.editarProvedores, name='editarProvedores'),
    path('procesarActualizacionProvedores/',views.procesarActualizacionProvedores, name='procesarActualizacionProvedores'),

    path('listadoProvedores/', views.listadoProvedores, name='listadoProvedores'),
    path('guardarProvedores/',views.guardarProvedores),
    path('eliminarProvedores/<idpo_SG>' ,views.eliminarProvedores),
    path('editarProvedores/<idpo_SG>' ,views.editarProvedores),
    path('procesarActualizacionProvedores/',views.procesarActualizacionProvedores),

    path('listadoVentas/', views.listadoVentas, name='listadoVentas'),
    path('guardarVentas/',views.guardarVentas),
    path('eliminarVentas/<idve_SG>' ,views.eliminarVentas),
    path('editarVentas/<idve_SG>' ,views.editarVentas),
    path('procesarActualizacionVentas/',views.procesarActualizacionVentas),

    path('listadoDetalles/', views.listadoDetalles, name='listadoDetalles'),
    path('guardarDetalles/',views.guardarDetalles),
    path('eliminarDetalles/<idde_SG>' ,views.eliminarDetalles),
    path('editarDetalles/<idde_SG>' ,views.editarDetalles),
    path('procesarActualizacionDetalles/',views.procesarActualizacionDetalles),

    path('listadoCategoria/', views.listadoCategoria, name='listadoCategoria'),
    path('guardarCategoria/',views.guardarProvedores),
    path('eliminarCategoria/<idca_SG>' ,views.eliminarCategoria),
    path('editarCategoria/<idca_SG>' ,views.editarCategoria),
    path('procesarActualizacionCategoria/',views.procesarActualizacionCategoria),

    path('enviar_correo/', views.enviar_correo, name='enviar_correo'),
    path('vistal/', views.vistal, name='vistal'),
]
