# Generated by Django 4.2.7 on 2024-02-06 13:27

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('HerramientasExpress', '0005_rename_provedores_productos_provedore'),
    ]

    operations = [
        migrations.RenameField(
            model_name='productos',
            old_name='provedore',
            new_name='provedor',
        ),
    ]
