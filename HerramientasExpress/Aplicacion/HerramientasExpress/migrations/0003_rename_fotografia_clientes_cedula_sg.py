# Generated by Django 4.2.7 on 2024-02-06 12:09

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('HerramientasExpress', '0002_clientes_fotografia'),
    ]

    operations = [
        migrations.RenameField(
            model_name='clientes',
            old_name='fotografia',
            new_name='cedula_SG',
        ),
    ]
